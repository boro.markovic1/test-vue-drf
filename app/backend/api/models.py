from unicodedata import name
from django.db import models

# Create your models here.


class Players(models.Model):
    name = models.CharField(max_length=50)
    club = models.CharField(max_length=30)
    goals = models.IntegerField()

    def __str__(self):
        return self.name