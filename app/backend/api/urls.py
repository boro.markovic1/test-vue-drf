from operator import index
from django.contrib import admin
from django.urls import path,include
from rest_framework.routers import DefaultRouter

from .views import *
urlpatterns = [
    path('list/',PlayersListAV.as_view(), name="players" ),
    path('<int:pk>/', PlayersDetAV.as_view(),name="player")
]
