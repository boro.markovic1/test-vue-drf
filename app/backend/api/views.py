from django.shortcuts import render
from rest_framework.response import Response
# from rest_framework.decorators import api_view
from rest_framework.views import APIView
from .models import *
from .serializers import *
from rest_framework.status import *
from rest_framework import generics, viewsets
from rest_framework import mixins
# Create your views here.

class PlayersListAV(APIView):
    def get(self,request):
        movie = Players.objects.all()
        serializer = PlayerSerializer(movie, many=True)
        return Response(serializer.data)

    def post(self,request):
        serializer = PlayerSerializer(data= request.data) 
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)
class PlayersDetAV(APIView):
    def get(self,request,pk):
        try:
            movie = Players.objects.filter(pk=pk)
        except Players.DoesNotExist:
            return Response({"Error: Movie not fount"}, status=HTTP_400_BAD_REQUEST)
        serializer = PlayerSerializer(movie, many=True)
        return Response(serializer.data)

    def put(self, request,pk):
        movie = Players.objects.get(pk=pk)
        serializer = PlayerSerializer(movie, data= request.data) 
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=HTTP_202_ACCEPTED)
        else:
            return Response(serializer.errors)
    def delete(self, request,pk):
        movie = Players.objects.get(pk=pk)
        movie.delete()
        return Response(status=HTTP_204_NO_CONTENT)
